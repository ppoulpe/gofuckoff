define(function(require){

    var clipboard = require('clipboard');
    var apiEndpoints = require('apiEndpoints');

    var resolveEndpoint = function(from, to) {

        let mapObj = {
            name: to,
            from: from ? from : 'Your best friend'
        };
    
        return apiEndpoints.getApiBaseUrl() +
            getRandomApiEndpoint(to)
                .replace(
                    /\b(?:name|from)\b/gi,
                    matched => mapObj[matched]
                );
    
    };

    var getRandomApiEndpoint = function(to){
        let filteredEndpoints = to ?
        apiEndpoints.getApiEndpoints().filter(element => element.includes("name")) :
        apiEndpoints.getApiEndpoints().filter(element => !element.includes("name"));

        return filteredEndpoints[Math.floor(Math.random() * filteredEndpoints.length)];
    };

    return {
        getFuckOffMessage: function (from, to) {

            var myHeaders = new Headers();
            myHeaders.append("Accept", "application/json");
        
            fetch(
                new Request(
                    resolveEndpoint(
                        document.getElementById('from').value,
                        document.getElementById('to').value
                    ),
                    {headers: myHeaders}
                )
            )
                .then(function (response) {
                    response.json().then(function (json) {
                        document
                            .getElementById('lovely-message')
                            .innerHTML = json.message;

                        document
                            .getElementById('your-best-friend')
                            .innerHTML = json.subtitle;

                        document.getElementById('fuckoff-container').classList.remove('is-hidden');
        
                        clipboard.init();
                    });
                })
                .catch(function(){
                    document
                        .getElementById('lovely-message')
                        .innerHTML = 'Somthing gone wrong. This website sucks. Fuck off!';
                })
        }
    };
});