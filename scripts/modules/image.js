define(
    [
        'https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.7/dist/html2canvas.min.js', 
        'require'
    ], 
    function(html2canvas, require){

        var modal = require('modal');

        return {
            exportToPNG: function(selector){
                html2canvas(
                    document.querySelector(selector), 
                    {logging: false}
                ).then(canvas => {
                    modal.open(
                        canvas.toDataURL('image/png')
                    );      
                });
            }
        };
    }
);
