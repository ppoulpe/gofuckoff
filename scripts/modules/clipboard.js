define(["https://cdn.jsdelivr.net/npm/clipboard@2.0.8/dist/clipboard.min.js"], function(ClipboardJS){

    var clipboard = new ClipboardJS('#copy-action');

    clipboard.on('success', function(e) {
        e.clearSelection();
    });

    clipboard.on('error', function(e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });

    return {
        init: function(){
            document
                .getElementById('toolbar')
                .classList
                .remove('is-hidden');

            document
                .getElementById('copy-action')
                .classList
                .remove('is-hidden');
        }
    };
});